
package com.axe.myweather.utils.glideload

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions


object GlideLoader {

    fun load(context: Context?, url: String, imageView: ImageView) {
        context?.let {
            Glide.with(it)
                    .load(url)
                    .apply(RequestOptions())
                    .into(imageView)
        }
    }

    fun load(context: Context?, resource: Int, imageView: ImageView) {
        context?.let {
            Glide.with(it)
                .load(resource)
                .apply(RequestOptions())
                .into(imageView)
        }

    }

}