package com.axe.myweather.utils

import com.axe.myweather.R
import java.text.NumberFormat
import java.util.*

object Constants{

    var cityName = "Bangkok"

    const val appId = "074bb904d7dfde5e20218335915b6b7f"

    //get icon from id weather
    fun getIcWeather(id : Int,icon : String) : Int?{
        return when (id){
            200 -> R.drawable.ic_01d
            201 -> R.drawable.ic_01d
            202 -> R.drawable.ic_01d
            210 -> R.drawable.ic_01d
            211 -> R.drawable.ic_01d
            212 -> R.drawable.ic_01d
            221 -> R.drawable.ic_01d
            230 -> R.drawable.ic_01d
            231 -> R.drawable.ic_01d
            232 -> R.drawable.ic_01d
            300 -> R.drawable.ic_09d
            301 -> R.drawable.ic_09d
            302 -> R.drawable.ic_09d
            310 -> R.drawable.ic_09d
            311 -> R.drawable.ic_09d
            312 -> R.drawable.ic_09d
            313 -> R.drawable.ic_09d
            314 -> R.drawable.ic_09d
            321 -> R.drawable.ic_09d
            500 -> R.drawable.ic_10d
            501 -> R.drawable.ic_10d
            502 -> R.drawable.ic_10d
            503 -> R.drawable.ic_10d
            504 -> R.drawable.ic_10d
            511 -> R.drawable.ic_13d
            520 ->  R.drawable.ic_09d
            522 ->  R.drawable.ic_09d
            531 ->  R.drawable.ic_09d
            600 -> R.drawable.ic_13d
            601 -> R.drawable.ic_13d
            602 -> R.drawable.ic_13d
            611 -> R.drawable.ic_13d
            612 -> R.drawable.ic_13d
            613 -> R.drawable.ic_13d
            615 -> R.drawable.ic_13d
            616 -> R.drawable.ic_13d
            620 -> R.drawable.ic_13d
            621 -> R.drawable.ic_13d
            622 -> R.drawable.ic_13d
            701 -> R.drawable.ic_50d
            711 ->  R.drawable.ic_50d
            721 ->  R.drawable.ic_50d
            731 ->  R.drawable.ic_50d
            741 ->  R.drawable.ic_50d
            751 ->  R.drawable.ic_50d
            761 -> R.drawable.ic_50d
            762 -> R.drawable.ic_50d
            771 ->  R.drawable.ic_50d
            781 ->  R.drawable.ic_50d
            800 -> if (icon == "01d")  R.drawable.ic_02d else if (icon == "01n") R.drawable.ic_02n else null
            801 -> if (icon == "02d")  R.drawable.ic_02d else if (icon == "02n") R.drawable.ic_02n else null
            802 -> if (icon == "03d") R.drawable.ic_03d else if (icon == "03n") R.drawable.ic_03n else null
            803 -> if (icon == "04d") R.drawable.ic_04d else if (icon == "04n") R.drawable.ic_04n  else null
            804 -> if (icon == "04d") R.drawable.ic_04d else if (icon == "04n") R.drawable.ic_04n  else null
            else -> null
        }
    }


    fun isCelsius(kelvin : Double,isCelsius : Boolean) : String{

        val format =  NumberFormat.getNumberInstance(Locale.US)
        format.maximumFractionDigits = 2
        format.minimumFractionDigits = 2

        val fahrenheit  = ((kelvin -273.15)*1.8 +32)

        return if (isCelsius) return "${format.format(kelvin - 273.15)} °C"
        else "${format.format(fahrenheit)} °F"
    }

    var digits_number = "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ]+"

}