package com.axe.myweather.ui.home

import android.app.Activity
import android.content.Context
import android.media.Image
import android.os.Bundle
import android.text.InputFilter
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.axe.myweather.BuildConfig
import com.axe.myweather.R
import com.axe.myweather.utils.Constants
import com.axe.myweather.utils.glideload.GlideLoader
import timber.log.Timber

class HomeActivity  : AppCompatActivity() {

    private lateinit var homeViewModel: HomeViewModel

    lateinit var ic_weather : ImageView
    lateinit var description : TextView
    lateinit var temp : TextView
    lateinit var humidity : TextView
    lateinit var switcher : SwitchCompat
    lateinit var search : EditText

    private var tempData : Double = 00.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acivity_home)
        initView()
        initViewModel()
        initSearch()
        initWeather()
        initShowError()

    }

    private fun initViewModel(){
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        // load api weather
        homeViewModel.getApiWeather(Constants.cityName)

    }

    private fun initSearch(){
        search.setText(Constants.cityName)
        search.imeOptions = EditorInfo.IME_ACTION_SEARCH
        setInput(search,Constants.digits_number)
        search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                homeViewModel.getApiWeather(search.text.toString())
                hideSoftKeyboard()
                true
            } else false
        })
    }

    private fun initView(){
        search = findViewById<EditText>(R.id.edt_search)
        ic_weather = findViewById<ImageView>(R.id.img_weather)
        description = findViewById<TextView>(R.id.tv_description)
        temp = findViewById<TextView>(R.id.tv_temp)
        humidity = findViewById<TextView>(R.id.tv_humidity)
        switcher = findViewById<SwitchCompat>(R.id.sw_temp)
    }

    private fun initWeather(){
        homeViewModel.watherData.observe(this@HomeActivity, Observer {
            val ic = Constants.getIcWeather(it.weather.first().id,it.weather.first().icon)
            // show icon weather
            ic?.let { it1 -> GlideLoader.load(this, it1,ic_weather) }
            // show temp
            tempData = it.main.temp
            temp.text = "Temp : ${Constants.isCelsius(tempData,false)}"
            //show humidity
            humidity.text = "Humidity : ${it.main.humidity}"
            // show description
            description.text = it.weather.first().description
            //

        })

        switcher.setOnCheckedChangeListener { compoundButton, isCheck ->
            if (isCheck) temp.text = "Temp : ${Constants.isCelsius(tempData,true)}"
            else temp.text = "Temp : ${Constants.isCelsius(tempData,false)}"
        }

    }

    private fun setInput(editText: EditText,Lang: String){
        editText.filters = arrayOf(InputFilter { source, start, end, dest, dstart, dend ->
            for (i in start until end) {
                if (source.equals("")) {
                    return@InputFilter source
                }else if (source.toString().trim().matches(Regex(Lang))) {
                    return@InputFilter source.trim()
                }else{
                    return@InputFilter ""
                }
            }
            null
        })
    }

    private fun initShowError(){
        homeViewModel.showError.observe(this, Observer {
            Toast.makeText(this@HomeActivity,"$it",Toast.LENGTH_LONG).show()
        })
    }
    fun hideSoftKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }




}