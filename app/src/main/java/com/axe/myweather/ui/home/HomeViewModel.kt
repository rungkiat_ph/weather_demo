package com.axe.myweather.ui.home

import androidx.lifecycle.viewModelScope
import androidx.lifecycle.ViewModel
import com.axe.myweather.service.repository.HomeRepository
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val homeRepository = HomeRepository()

    var watherData = homeRepository.getWeather
    var showError = homeRepository.showError


    fun getApiWeather(cityName : String){ // memberId encrypt
        viewModelScope.launch { homeRepository.getApiWeather(cityName) }
    }

}