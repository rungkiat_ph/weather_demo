package com.axe.myweather.service.repository

import android.content.Context
import com.axe.myweather.model.WeatherModel
import com.axe.myweather.service.RetrofitMainManager
import com.axe.myweather.utils.Constants
import com.axe.myweather.utils.SingleLiveEvent
import timber.log.Timber


class HomeRepository {

    private val apiService = RetrofitMainManager.apiService
    val getWeather = SingleLiveEvent<WeatherModel>()
    val showError = SingleLiveEvent<String>()

    suspend fun getApiWeather(cityName : String){
        try {
            val response = apiService.getWeather(cityName,Constants.appId)
            if (response.isSuccessful) {
                if (response.body()?.cod == 200){
                    getWeather.value = response.body()
                }else{
                    showError.value = response.message()
                }
            }else{
                showError.value = response.message()
            }
        }catch (e : Exception){
            showError.value = e.message
        }
    }




}