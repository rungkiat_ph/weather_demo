package com.axe.myweather.service

import com.axe.myweather.model.WeatherModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface Weather {

    @GET("weather?")
    suspend fun getWeather(@Query("q") cityName: String,
                           @Query("appid") appId: String): Response<WeatherModel>

}