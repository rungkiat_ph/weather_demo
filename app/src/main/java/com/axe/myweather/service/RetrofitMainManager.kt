package com.axe.myweather.service

import com.axe.myweather.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitMainManager {

    val apiService : Weather

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
        client.addInterceptor(loggingInterceptor)
        client.connectTimeout(60, TimeUnit.SECONDS)   // connect timeout
        client.readTimeout(60, TimeUnit.SECONDS)     // socket timeout

        client.addInterceptor {
            val original = it.request()
            val requestBuilder = original.newBuilder()
            requestBuilder.header("Content-Type", "application/json")
            val request = requestBuilder.method(original.method, original.body).build()
            return@addInterceptor it.proceed(request)
        }

        apiService = Retrofit.Builder()
            .baseUrl(BuildConfig.Base_Url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
            .create(Weather::class.java)

    }

}