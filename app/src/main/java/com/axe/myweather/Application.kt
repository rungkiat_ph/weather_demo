package com.axe.myweather

import android.app.Application
import com.axe.myweather.service.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class Application : Application() {


    override fun onCreate() {
        super.onCreate()
        initTimber()
        initKoinModules()

    }
    private fun initTimber() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return super.createStackElementTag(element) + ": ${element.methodName} : ${element.lineNumber} "
            }
        })
    }

    private fun initKoinModules() {
        // Adding Koin modules to our application
        startKoin {
            androidContext(this@Application)
            modules(listOf(appModule))
        }
    }



}
